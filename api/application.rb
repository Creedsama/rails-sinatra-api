# require 'rubygems'
require 'sinatra'
require 'sinatra/json'
require 'data_mapper'
require 'dm-migrations'
require 'json'
require 'bundler'

Bundler.require

configure :development do
	DataMapper::Logger.new($stdout, :debug)
	DataMapper::setup(:default, 'mysql://root:testing@localhost/todo')
end

configure :production do
	DataMapper::setup(:default, 'mysql://root:testing@localhost/todo')
end


require './models/init'
require './routes/init'

DataMapper.finalize

if Task.count == 0
	Task.create(title: "First", body: "Our first Todo.")
end