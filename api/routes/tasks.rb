get '/' do
	"Hello World"
end

get '/api/tasks' do
	content_type :json

	tasks = Task.all
	tasks.to_json
end

get '/api/tasks/:id' do
	content_type :json

	task = Task.get params[:id]
	task.to_json
end

post '/api/tasks' do
	content_type :json

	task = Task.new params[:task]
	if task.save
		task.to_json
	else
		status 500
		json task.errors.full_messages
	end
end

put '/api/tasks/:id' do
	content_type :json

	task = Task.get params[:id]
	if task.update params[:task]
		status 200
		# json "Task was updated."
	else
		status 500
		json task.errors.full_messages
	end
end

delete '/api/tasks/:id' do
	content_type :json

	task = Task.get params[:id]
	if task.destroy
		status 200
		# json "Task was removed"
	else
		status 500
		json "There was a problem removing the task."
	end
end
