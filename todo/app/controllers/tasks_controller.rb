class TasksController < ApplicationController

	before_action :set_task, only: [:show, :edit, :update ,:destroy]

	def index
		@tasks = Task.all
		respond_to do |format|
			format.html
		end
	end

	def show
		respond_to do |format|
			format.html
		end
	end

	def new
		@task = Task.new
		respond_to do |format|
			format.html
		end
	end

	def create
		@task = Task.new(task_params)

		respond_to do |format|
            if @task.save!
                format.html { redirect_to tasks_url, notice: 'Task was successfully created.' }
                format.json { render :show, status: :created, location: @task }
            else
                format.html { render :new }
                format.json { render json: @task.errors, status: :unprocessable_entity }
            end
        end
	end

	def edit
		# @task = Task.find(params[:id])
		respond_to do |format|
			format.html
		end
	end

	def update
		respond_to do |format|
			@task.attributes = params[:task]
            if @task.save
                format.html { redirect_to @task, notice: 'Task was successfully updated.' }
                format.json { render :show, status: :ok, location: @task }
            else
                format.html { render :edit }
                format.json { render json: @task.errors, status: :unprocessable_entity }
            end
        end
	end

	def destroy
		respond_to do |format|
	        if @task.destroy
	        	format.html { redirect_to tasks_url , notice: 'Task was successfully destroyed.' }
	        	format.json { render :index, status: :ok, location: @task }
	        end
        	# redirect_to tasks_url
        end
    end

    private

    def set_task
        @task = Task.find(params[:id])
    end

    def task_params
        params.require(:task).permit(:title, :body)
    end
end
