Her::API.setup url: "http://0.0.0.0:4567/api" do |c|
	# Request
	c.use Faraday::Request::UrlEncoded

	# Response
	c.use Her::Middleware::DefaultParseJSON

	# Adapter
	c.use Faraday::Adapter::NetHttp
end